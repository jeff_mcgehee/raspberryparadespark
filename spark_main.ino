// This #include statement was automatically added by the Spark IDE.
#include "OneWire/OneWire.h"

// This #include statement was automatically added by the Spark IDE.
#include "spark-dallas-temperature/spark-dallas-temperature.h"

String LOCATION = "living+room"; //The location of your sensor

TCPClient client;
String temp;
String reqString;
String reqString1;
String reqString2;
char testChar[5];

#define server "YOUR_SERVER_URL" //www.yoursite.com

DallasTemperature dallas(new OneWire(D0));

void setup()
{
    RGB.control(true);
    RGB.color(0, 0, 0);

  Serial.begin(9600);

    dallas.begin();

    int count=0;
    while(count<100){
        Spark.process();
        count++;
    }

    Serial.flush();


}

void loop()
{
    dallas.requestTemperatures();
    float fahrenheit = dallas.getTempCByIndex( 0 )*9.0/5.0 + 32.0;

    Serial.print("Temperature: "); Serial.println(fahrenheit);
    delay(500);
    Serial.flush();
if (client.connect(server, YOUR_SERVER_PORT))
  {



    reqString1 = "GET /_sparkTest/SPARK_MODULE_ID/";
    //Should be an int that matches the sensor in your MySQL DB

    reqString2 = " HTTP/1.1\n";

    client.print(reqString1);
    client.print(LOCATION);
    client.print("/");
    client.print(fahrenheit);

    client.print(reqString2);
    client.println("Host: HOST_URL"); //www.yoursite.com:port

    client.println("Authorization: Basic BASE64_AUTH_CREDENTIALS");
    //You must encode your credentials to base64

    client.println();


    delay(500);
    Serial.println(client.read());
    client.flush();
    client.stop();

    Spark.sleep(SLEEP_MODE_DEEP,60);

  }
}
